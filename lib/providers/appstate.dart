import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';

class AppState with ChangeNotifier, DiagnosticableTreeMixin {
  final LocalStorage storage = new LocalStorage('viewer_offline_grade');
  Map get grade => storage.getItem('grade');

  setGrade(grade) {
    storage.setItem('grade', grade);
    notifyListeners();
  }
}
