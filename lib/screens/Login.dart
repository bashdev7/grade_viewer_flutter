import 'dart:collection';
import 'dart:convert';

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:grade_viewer_flutter/datasource/grade_collections.dart';
import 'package:grade_viewer_flutter/providers/appstate.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:rive/rive.dart';

class Login extends StatefulWidget {
  Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final riveFileName = 'assets/college_mascot.riv';
  Artboard _artboard;
  SimpleAnimation _idle, _defaultEyes, _cuteEyes;
  String _courseValue;
  String _sectionValue;
  String _subjectValue;
  Map _data;
  String idNumber;
  String errorMessage;
  final _formKey = GlobalKey<FormState>();

  bool _doingDefaultEyes = false;
  bool _doingCuteEyes = false;

  bool _isIdle = true;

  int yearToday = DateTime.now().year;

  @override
  void initState() {
    _loadRiveFile();
    super.initState();
  }

  void onActive() {
    setState(() {
      _idle.isActive = _isIdle = false;
      _defaultEyes.isActive = _doingDefaultEyes = true;
    });
  }

  void onSleep() {
    _reset(_idle);
    setState(() {
      _idle.isActive = _isIdle = true;
      _defaultEyes.isActive = _doingDefaultEyes = false;
      _cuteEyes.isActive = _doingCuteEyes = false;
    });
  }

  void onFail() {
    print("Onfail triggered");
    setState(() {
      _idle.isActive = _isIdle = false;
      _defaultEyes.isActive = _doingDefaultEyes = false;
      _cuteEyes.isActive = _doingCuteEyes = true;
    });
  }

  getGrades(
      String id, String course, String yearSection, String subject) async {
    setState(() {
      errorMessage = null;
    });
    Map source = GradeCollections().entries.singleWhere((element) {
      return element["id"] == "${subject}_${course}_$yearSection";
    });

    await http.get(Uri.https("api.npoint.io", source['api']),
        headers: {"Content-Type": "application/json"}).then((value) {
      String grades = value.body.toString();
      // print(grades);
      List decoded = (json.decode(grades) as List);
      Map filtered = decoded.asMap().entries.singleWhere((element) {
        return element.value['1 - ID Number'] == id.toUpperCase();
      }).value;

      var sorted = SplayTreeMap<String,dynamic>.from(filtered, (a,b)=>int.parse(a.toString().split("-")[0]).compareTo(int.parse(b.toString().split("-")[0])));

      context.read<AppState>().setGrade(sorted);
     
      setState(() {
        _data = sorted;
      });
    }).catchError((error) {
      print(error.toString());
      onFail();
      setState(() {
        errorMessage = "Record could not be found. Please try again.";
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    // double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SingleChildScrollView(
        child: Center(
          child: Container(
              width: 300,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Center(
                    child: Container(
                      width: 300,
                      height: 250,
                      child: _artboard != null
                          ? Rive(
                              artboard: _artboard,
                              fit: BoxFit.cover,
                            )
                          : Center(
                              child: CircularProgressIndicator(),
                            ),
                    ),
                  ),
                  Form(
                      key: _formKey,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            FadeInRight(
                              child: Text(
                                "Grade Viewer v2.0",
                                textAlign: TextAlign.left,
                                style: TextStyle(fontSize: 20),
                              ),
                            ),
                            SizedBox(height: 10),
                            FadeInRight(
                              delay: Duration(milliseconds: 300),
                              child: FocusScope(
                                onFocusChange: (value) {
                                  if (!value) {
                                  } else {
                                    onActive();
                                  }
                                },
                                child: TextFormField(
                                  onChanged: (value) {
                                    setState(() {
                                      idNumber = value;
                                    });
                                  },
                                  decoration:
                                      InputDecoration(labelText: "ID Number"),
                                ),
                              ),
                            ),
                            FadeInRight(
                              delay: Duration(milliseconds: 500),
                              child: Container(
                                  width: 300,
                                  child: DropdownButton<String>(
                                    isExpanded: true,
                                    value: _subjectValue,
                                    icon: Icon(Icons.arrow_downward),
                                    iconSize: 24,
                                    elevation: 16,
                                    hint: Text("Subject"),
                                    onChanged: (String newValue) {
                                      onActive();
                                      setState(() {
                                        _subjectValue = newValue;
                                      });
                                    },
                                    items: <String>[
                                      'cit245',
                                      'da302',
                                      'da303',
                                      'emc215',
                                      'da307'
                                      // 'emc208',
                                      // 'emc202',
                                      // 'emc215',
                                      // 'emc203',
                                      // 'emc204',
                                      // 'ccs221',
                                      // 'ccs222',
                                      // 'cc206'
                                    ].map<DropdownMenuItem<String>>(
                                        (String value) {
                                      return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value.toUpperCase()));
                                    }).toList(),
                                  )),
                            ),
                            FadeInRight(
                              delay: Duration(milliseconds: 700),
                              child: Container(
                                  width: 300,
                                  child: DropdownButton<String>(
                                    isExpanded: true,
                                    value: _courseValue,
                                    icon: Icon(Icons.arrow_downward),
                                    iconSize: 24,
                                    elevation: 16,
                                    hint: Text("Course"),
                                    onChanged: (String newValue) {
                                      onActive();
                                      setState(() {
                                        _courseValue = newValue;
                                      });
                                    },
                                    items: <String>[
                                      'bsit',
                                      'bsemc'
                                    ].map<DropdownMenuItem<String>>(
                                        (String value) {
                                      return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value.toUpperCase()));
                                    }).toList(),
                                  )),
                            ),
                            FadeInRight(
                              delay: Duration(milliseconds: 900),
                              child: Container(
                                  width: 300,
                                  child: DropdownButton<String>(
                                    isExpanded: true,
                                    value: _sectionValue,
                                    icon: Icon(Icons.arrow_downward),
                                    iconSize: 24,
                                    elevation: 16,
                                    hint: Text("Year and Section"),
                                    onChanged: (String newValue) {
                                      onActive();
                                      setState(() {
                                        _sectionValue = newValue;
                                      });
                                    },
                                    items: <String>[
                                      '3a',
                                      '4a',
                                      '4ab',
                                    ].map<DropdownMenuItem<String>>(
                                        (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value.toUpperCase()),
                                      );
                                    }).toList(),
                                  )),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                          ])),
                  errorMessage != null
                      ? Column(
                          children: [
                            Text(
                              errorMessage != null ? errorMessage : "",
                              style: TextStyle(color: Colors.red),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                          ],
                        )
                      : SizedBox(),
                  FadeInRight(
                    delay: Duration(milliseconds: 1000),
                    child: MaterialButton(
                        minWidth: screenWidth,
                        color: Colors.orange,
                        child: Text(
                          'View',
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          getGrades(idNumber, _courseValue, _sectionValue,
                                  _subjectValue)
                              .then((value) {
                            if (_data != null) {
                              Navigator.pushNamed(context, '/viewer');
                            }
                          });
                        }),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    "$yearToday | </> && <3 && Flutter",
                    style: TextStyle(color: Colors.grey),
                  ),
                  Text(
                    "mjsolidarios@wvsu.edu.ph",
                    style: TextStyle(color: Colors.grey),
                  ),
                ],
              )),
        ),
      ),
    );
  }

  void _reset(SimpleAnimation animation) {
    animation.instance.time = (animation.instance.animation.enableWorkArea
                ? animation.instance.animation.workStart
                : 0)
            .toDouble() /
        animation.instance.animation.fps;
  }

  // loads a Rive file
  void _loadRiveFile() async {
    final bytes = await rootBundle.load(riveFileName);
    final file = RiveFile();

    if (file.import(bytes)) {
      Artboard baseArtboard = file.mainArtboard;
      //Intialize animations
      baseArtboard.addController(_idle = SimpleAnimation('idle'));
      baseArtboard
          .addController(_defaultEyes = SimpleAnimation('default-eyes'));
      baseArtboard.addController(_cuteEyes = SimpleAnimation('cute-eyes'));
      _idle.isActive = _isIdle;
      _defaultEyes.isActive = _doingDefaultEyes;
      _cuteEyes.isActive = _doingCuteEyes;

      setState(() => _artboard = baseArtboard);
    } else {
      print("Error loading file.");
    }
  }
}
