import 'package:animate_do/animate_do.dart';
import 'package:emojis/emojis.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:grade_viewer_flutter/providers/appstate.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:provider/provider.dart';

class Viewer extends StatefulWidget {
  Viewer({Key key}) : super(key: key);

  @override
  _ViewerState createState() => _ViewerState();
}

class _ViewerState extends State<Viewer> {
  @override
  Widget build(BuildContext context) {
    Map grade = context.read<AppState>().grade;
    if (grade == null) {
      return Scaffold(
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Loading data..."),
              SizedBox(
                height: 10,
              ),
              MaterialButton(
                color: Colors.orange,
                child: Text(
                  'Try Again',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/login');
                },
              )
            ],
          ),
        ),
      );
    }
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Container(
              width: 300,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 40,
                    ),
                    Text(
                      "Grade Summary",
                      style: TextStyle(fontSize: 30),
                    ),
                    SizedBox(height: 20),
                    Container(
                      child: ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: grade == null ? 0 : grade.keys.length,
                        itemBuilder: (BuildContext context, int index) {
                          dynamic item = grade[grade.keys.toList()[index]];
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              FadeInRight(
                                delay:
                                    Duration(milliseconds: 500 + (index * 100)),
                                child: Text(
                                  "${grade.keys.toList()[index]}",
                                  style: TextStyle(fontSize: 15),
                                ),
                              ),
                              grade.keys.toList()[index].split("-")[1] == "Midterm 40% + Finals 60%"
                                  ? FadeInRight(
                                      delay: Duration(
                                          milliseconds: 500 + (index * 100)),
                                      child: Container(
                                        padding: EdgeInsets.only(top: 5),
                                        child: new LinearPercentIndicator(
                                          animation: true,
                                          animationDuration: 3000,
                                          width: 300.0,
                                          lineHeight: 20,
                                          percent: (item as double) / 100,
                                          center: Text(
                                            "${item != 'null' ? item : '0'}",
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                          progressColor: Colors.orange,
                                        ),
                                      ),
                                    )
                                  : grade.keys.toList()[index].split("-")[1] == "GPA"
                                      ? FadeInRight(
                                          delay: Duration(
                                              milliseconds:
                                                  500 + (index * 250)),
                                          child: Markdown(
                                              styleSheet: MarkdownStyleSheet(
                                                  textScaleFactor: 1.5),
                                              shrinkWrap: true,
                                              data: item.toString()),
                                        )
                                      : item.toString().length > 0
                                          ? FadeInRight(
                                              delay: Duration(
                                                  milliseconds:
                                                      500 + (index * 300)),
                                              child: item.toString() == 'null'
                                                  ? Text(
                                                      "I'm glad that you made it through this subject! Stay awesome!",
                                                      style: TextStyle(
                                                          fontSize: 20))
                                                  : Markdown(
                                                      styleSheet:
                                                          MarkdownStyleSheet(
                                                              textScaleFactor:
                                                                  1.5),
                                                      shrinkWrap: true,
                                                      data: item.toString()),
                                            )
                                          : FadeInRight(
                                              delay: Duration(
                                                  milliseconds:
                                                      500 + (index * 350)),
                                              child: Text(
                                                "I'm glad that you made it through this subject! Stay awesome!",
                                                style: TextStyle(fontSize: 20),
                                              ),
                                            ),

                              // Text("${item != 'null' ? item : 'Stay Awesome!'}",
                              //     style: TextStyle(fontSize: 20)),
                              SizedBox(
                                height: 5,
                              ),
                              Divider()
                            ],
                          );
                        },
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      "Thanks!",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                        "* To my students for patiently waiting for their grades ${Emojis.smilingFaceWithSmilingEyes}."),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                        "* FlutterPH Dev repo for the coding guide ${Emojis.glasses}."),
                    SizedBox(
                      height: 40,
                    ),
                    FadeInRight(
                      delay: Duration(milliseconds: 1000),
                      child: MaterialButton(
                          minWidth: 250,
                          color: Colors.orange,
                          child: Text(
                            'Logout',
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () {
                            Navigator.pushNamed(context, '/login');
                          }),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                  ])),
        ),
      ),
    );
  }
}
