import 'package:flutter/material.dart';
import 'package:grade_viewer_flutter/providers/appstate.dart';
import 'package:grade_viewer_flutter/screens/Login.dart';
import 'package:grade_viewer_flutter/screens/Viewer.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(
        create: (_) => AppState(),
      )
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Grade Viewer',
      theme: ThemeData(
        primarySwatch: Colors.orange,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Login(),
      routes: <String, WidgetBuilder>{
        '/login': (BuildContext context) => Login(),
        '/viewer': (BuildContext context) => Viewer()
      },
    );
  }
}
