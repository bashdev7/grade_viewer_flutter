import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:grade_viewer_flutter/models/student.dart';

abstract class IStudentsLocalDataSource {
  Future<List<Student>> getGrades();
}

class StudentsLocalDataSource implements IStudentsLocalDataSource {
  @override
  Future<List<Student>> getGrades() async {
    try {
      String data = await rootBundle.loadString('data/ccs222_csa.json');
      final result = json.decode(data);
      return List<Student>.from(
        result.map(
          (i) {
            return Student.fromJson(i);
          },
        ),
      );
    } catch (e) {
      return [];
    }
  }
}
